import gendomain.*
import geom.Triangle;

class TsogkaAcoust2D{
	String domName="someAcousticDomain"
	Domain theDomain
	double x0,y0
	double Lx,Ly
	double hx,hy
	int Nx,Ny
	int BLid,BRid,TLid,TRid
	int storeStep=1

	//double[][] blk,rho
	double blk,rho
	double[][] Vxd, Vxu, Vyl, Vyr, Prs
	
	int Nt,Nst
	double Dt,Dt2

	boolean BodyForces=false
	double[] g
	double[][] f
	int rstep=0
	
	TsogkaAcoust2D(){}

	TsogkaAcoust2D(double x0,double y0,double Lx,double Ly,int Nx,int Ny,double rhov,double blkv,int storeStep=1){
		theDomain=new Domain();
		this.x0=x0
		this.y0=y0
		this.storeStep=storeStep;
		this.Lx=Lx
		this.Ly=Ly
		this.Nx=Nx
		this.Ny=Ny
		hx=Lx/(Nx-1)
		hy=Ly/(Ny-1)
		
		printf ("Rect size Lx= %f , Ly= %f.\n",Lx, Ly);
		printf ("Mesh size hx= %f , hy= %f.\n",hx, hy);
		printf ("Mesh numb Nx= %d , Ny= %d.\n",Nx, Ny);
		BLid=1
		BRid=BLid+Nx-1
		TLid=(Ny-1)*Nx+1
		TRid=TLid+Nx-1
		println("Corner nodes: "+BLid+" "+BRid+" "+TLid+" "+TRid)
		
		//blk=new double[Nx-1][Ny-1]
		//rho=new double[Nx-1][Ny-1]
		this.rho=rhov
		this.blk=blkv

		Vxd=new double[Nx][Ny]
		Vxu=new double[Nx][Ny]
		Vyl=new double[Nx][Ny]
		Vyr=new double[Nx][Ny]
		Prs=new double[Nx-1][Ny-1]
	}

	void setName(String Name){this.domName=Name}

	double computeDt(){
		double Vp=sqrt(blk/rho)
		printf (domName+", Pressure wave speed= %f.\n",Vp);
		double al=sqrt(2.0)/2.0
		double Dt=al/Vp*min(hx,hy)
		return Dt
	}

	void setSpaceTimeNodes(int Nt,int Nst,double Dt,double Dt2, int ndofs=2){
		this.Nt=Nt
		this.Nst=Nst
		this.Dt=Dt
		this.Dt2=Dt2
		// setup nodes for domain
		int id=0;
		for (int j=1;j<=Ny;j++){
			double ycoord=(j-1)*hy+y0
			for (int i=1;i<=Nx;i++){
			      double xcoord=(i-1)*hx+x0
			      Node aNode=new Node(++id,xcoord,ycoord)
			      aNode.setNumDOFs_Steps(ndofs,Nst+1)
			      if(i==1||i==Nx||j==1||j==Ny)aNode.setBoundary() // set nodes belong to boundary
			      theDomain.putNode(aNode)
		      }
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	Closure setVLC={double[][] Str->
		double[] sol=new double[Nx*Ny]
		int count=0
		int i,j
		(0..<Ny).each{
			j=it
			(0..<Nx).each{
				i=it
				sol[count++]=Str[i][j]
			}
		}
		//Matrix(Str).print()
		//println sol
		return sol
	}
	
	Closure setPRS={double[][] varr ->
		int i,j
		double[] sol=new double[Nx*Ny]
		(0..<Ny-1).each{
			j=it
			(0..<Nx-1).each{
				i=it
				int k1=j*Nx+i
				int k2=k1+1
				int k3=k1+Nx
				int k4=k3+1
				sol[k1]+=varr[i][j]
				sol[k2]+=varr[i][j]
				sol[k3]+=varr[i][j]
				sol[k4]+=varr[i][j]
			}
		}
		int count=-1
		(0..<Ny).each{
			j=it
			(0..<Nx).each{
				i=it
				count++
				if(i==0||i==Nx-1||j==0||j==Ny-1){
					switch(i){
						case 0 : if(j==0||j==Ny-1){
							}else{
								sol[count]/=2.0;
								} 
								break;
						case (Nx-1) : 
						if(j==0||j==Ny-1){
						}else{
							sol[count]/=2.0;
						} 
						break;
					}
	
					switch(j){
						case 0 : if(i==0||i==Nx-1){
							}else{
								sol[count]/=2.0;
								} 
								break;
						case (Ny-1) : 
						if(i==0||i==Nx-1){
						}else{
							sol[count]/=2.0;
						} 
						break;
					}
				}else{
					sol[count]/=4.0
				}
			}
		}
		//Matrix(varr).print()
		//println("-------------------------------  -------------------------------")
		//println sol
		return sol
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	void doStep(int step, boolean store){
		// pressure
		Closure doPressure={
			int i,j
			(0..<Nx-1).each{
				i=it
				(0..<Ny-1).each{
					j=it
					double cx=0.5*blk*Dt/hx; double cy=0.5*blk*Dt/hy
					Prs[i][j]=Prs[i][j]+cx*(Vxd[i+1][j+1]-Vxd[i][j+1]+Vxu[i+1][j]-Vxu[i][j])+cy*(Vyl[i+1][j+1]-Vyl[i+1][j]+Vyr[i][j+1]-Vyr[i][j])
					if(BodyForces)Prs[i][j]+=g[step]*f[i][j]*Dt*blk
	
				}
			}
		}
		// velocity
		Closure doVelocity={
			int i,j
			(1..<Nx-1).each{
				i=it
				(1..<Ny-1).each{
					j=it
					Vxu[i][j]=Vxu[i][j]+(Dt/(hx*rho))*(Prs[i][j]-Prs[i-1][j])
					Vxd[i][j]=Vxd[i][j]+(Dt/(hx*rho))*(Prs[i][j-1]-Prs[i-1][j-1])
					Vyr[i][j]=Vyr[i][j]+(Dt/(hy*rho))*(Prs[i][j]-Prs[i][j-1])
					Vyl[i][j]=Vyl[i][j]+(Dt/(hy*rho))*(Prs[i-1][j]-Prs[i-1][j-1])
				}
			}
			doBoundaryRelations(step)
		}
		
		doVelocity()
		doPressure()
		doCloseStep(step)
		if(store)doStoreResponse(step)
	}

	void configureLoading(){
		//println domName+"original configureLoading"
	}
	
	void doCloseStep(int step){}

	void doBoundaryRelations(int step){}

	void doStoreResponse(int step){
		if(step%storeStep == 0){
			rstep++
			theDomain.updateNodesDOF(setPRS(Prs),rstep,2)
			theDomain.updateNodesDOF(setVLC(Vxu),rstep,1)
			//theDomain.updateNodesDOF(setVLC(Vxd),rstep,2)
			theDomain.updateNodesDOF(setVLC(Vyl),rstep,0)
			//theDomain.updateNodesDOF(setVLC(Vyr),rstep,4)
		}
	}

	void triangulate(){
		java.util.LinkedHashMap trng=[:]
		int indtr=0
		boolean right=true
		(1..<Nx).each{
			int ix=it
			(1..<Ny).each{
				int iy=it
				int nc=(iy-1)*Nx
				Node[] nds = new Node[3];
				right=true; if(random()<0.5)right=false
				if(right){
						nds[0]=theDomain.getNode(nc+ix); nds[1]=theDomain.getNode(nc+ix+1); nds[2]=theDomain.getNode(nc+ix+1+Nx)
				}else{
						nds[0]=theDomain.getNode(nc+ix); nds[1]=theDomain.getNode(nc+ix+1); nds[2]=theDomain.getNode(nc+ix+Nx)
					}
				Triangle aTrg = new Triangle(++indtr,nds)
				trng.put(aTrg.getID(), aTrg)
		
				nds = new Node[3];
				if(right){
						nds[0]=theDomain.getNode(nc+ix+Nx); nds[2]=theDomain.getNode(nc+ix+Nx+1); nds[1]=theDomain.getNode(nc+ix)
						//right=false
					}else{
						nds[0]=theDomain.getNode(nc+ix+Nx+1); nds[1]=theDomain.getNode(nc+ix+Nx); nds[2]=theDomain.getNode(nc+ix+1)
						//right=true
					}
				aTrg = new Triangle(++indtr,nds)
				trng.put(aTrg.getID(), aTrg)
			}
		}
		theDomain.setShapes(trng)
	}
}

new TsogkaAcoust2D()

