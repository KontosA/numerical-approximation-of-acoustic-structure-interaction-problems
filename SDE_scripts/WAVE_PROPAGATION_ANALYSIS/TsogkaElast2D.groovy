import gendomain.*
import geom.Triangle;

class TsogkaElast2D{
	String domName="someElasticDomain"
	Domain theDomain
	double x0,y0
	double Lx,Ly
	double hx,hy
	int Nx,Ny
	double[][] c11,c12,c13,c22,c23,c33
	double[][] a11,a12,a13,a22,a23,a33
	///double[][] elast,pois,rho
	double elast,pois,rho
	int BLid,BRid,TLid,TRid
	int storeStep
	double[][] Sxxh,Sxxb,Syyd,Syyg,Sxy
	
	double[][] Vx,Vy
	
	double[][] Dx,Dy
	int Nt,Nst
	double Dt,Dt2

	boolean BodyForces=false
	double[] g
	double[][] fx 
	double[][] fy 
	int rstep=0

	boolean LameCoefs=true
	
	TsogkaElast2D(){}

	TsogkaElast2D(double x0,double y0,double Lx,double Ly,int Nx,int Ny,double rhov,double elastv,double poisv,int storeStep){
		println("Lame's coefficients: "+LameCoefs)
		theDomain=new Domain();
		this.x0=x0
		this.y0=y0
		this.storeStep=storeStep;
		this.Lx=Lx
		this.Ly=Ly
		this.Nx=Nx
		this.Ny=Ny
		this.hx=Lx/(Nx-1)
		this.hy=Ly/(Ny-1)
		printf ("Rect size Lx= %f , Ly= %f.\n",Lx, Ly);
		printf ("Mesh size hx= %f , hy= %f.\n",hx, hy);
		printf ("Mesh numb Nx= %d , Ny= %d.\n",Nx, Ny);
		BLid=1
		BRid=BLid+Nx-1
		TLid=(Ny-1)*Nx+1
		TRid=TLid+Nx-1
		println("Corner nodes: "+BLid+" "+BRid+" "+TLid+" "+TRid)

		this.rho = rhov;
		this.elast =elastv
		this.pois =poisv

		c11 = new double[Nx][Ny]
		c12 = new double[Nx][Ny]
		c13 = new double[Nx][Ny]
		c22 = new double[Nx][Ny]
		c23 = new double[Nx][Ny]
		c33 = new double[Nx][Ny]
		
		a11 = new double[Nx][Ny]
		a12 = new double[Nx][Ny]
		a13 = new double[Nx][Ny]
		a22 = new double[Nx][Ny]
		a23 = new double[Nx][Ny]
		a33 = new double[Nx][Ny]
		
		//(0..<Ny).each{
		//	int j=it
		//	(0..<Nx).each{
		//		int i=it
		//		elast[i][j]=elastv; pois[i][j]=poisv
		//	}
		//}
		
		(0..<Ny).each{
			int j=it
			(0..<Nx).each{
				int i=it
				if(!LameCoefs){
					c11[i][j]=elast/(1-pois*pois)//lam[i][j]+2.0*mu[i][j]
					c12[i][j]=pois*elast/(1-pois*pois)//lam[i][j]
					c13[i][j]= 0.0;
					c22[i][j]=elast/(1-pois*pois)//lam[i][j]+2.0*mu[i][j]
					c23[i][j]= 0.0;
					c33[i][j]=0.5*(1-pois)*elast/(1-pois*pois)//lam[i][j]+2.0*mu[i][j]
				}else{
					// here elast= lam; pois=mu
					c11[i][j]=elast+2.0*pois//lam[i][j]+2.0*mu[i][j]
					c12[i][j]=elast//lam[i][j]
					c13[i][j]= 0.0;
					c22[i][j]=elast+2.0*pois//lam[i][j]+2.0*mu[i][j]
					c23[i][j]= 0.0;
					c33[i][j]=pois//lam[i][j]+2.0*mu[i][j]
				}
			}
		}
		
		(0..<Ny).each{
			int j=it
			(0..<Nx).each{
				int i=it
				double det=-c11[i][j]*c22[i][j]*c33[i][j]+c11[i][j]*c23[i][j]*c23[i][j]+c12[i][j]*c12[i][j]*c33[i][j]+c13[i][j]*c13[i][j]*c22[i][j]-2.0*c12[i][j]*c13[i][j]*c23[i][j]
		
				a11[i][j]=-(c22[i][j]*c33[i][j]-c23[i][j]*c23[i][j])/det
				a12[i][j]= (c12[i][j]*c33[i][j]-c13[i][j]*c23[i][j])/det
				a13[i][j]=-(c12[i][j]*c23[i][j]-c13[i][j]*c22[i][j])/det
				a22[i][j]= (-c11[i][j]*c33[i][j]+c13[i][j]*c13[i][j])/det
				a23[i][j]=-(-c11[i][j]*c23[i][j]+c12[i][j]*c13[i][j])/det
				a33[i][j]= (-c11[i][j]*c22[i][j]+c12[i][j]*c12[i][j])/det
			}
		}
		
		Sxxh=new double[Nx][Ny]
		Sxxb=new double[Nx][Ny]
		Syyd=new double[Nx][Ny]
		Syyg=new double[Nx][Ny]
		Sxy=new double[Nx][Ny]
		
		Vx=new double[Nx-1][Ny-1]
		Vy=new double[Nx-1][Ny-1]
		
		Dx=new double[Nx-1][Ny-1]
		Dy=new double[Nx-1][Ny-1]
	}

	void setName(String Name){this.domName=Name}

	double computeDt(){
		double v1=Double.NEGATIVE_INFINITY; double v2=v1; double v3=v1; double Vp=v1; double Vs=v1
		(0..<Nx).each{
			int i=it
			(0..<Ny).each{
				int j=it
				if(c11[i][j]>v1)v1=c11[i][j]
				if(c22[i][j]>v2)v2=c22[i][j]
				if(c33[i][j]>v3)v3=c33[i][j]
			}
		}
		v1=sqrt(v1/rho); v2=sqrt(v2/rho); v3=sqrt(v3/rho)
		Vp=max(v1,max(v2,v3))
		Vs=min(v1,min(v2,v3))
		
		printf (domName+", Pressure wave speed= %f , Shear wave speed= %f.\n",Vp, Vs);
		double al=1.0
		//al=sqrt(2.0)/2.0
		double Dt=al/Vp*min(hx,hy); //Dt/=10.0
		return Dt
	}

	void setSpaceTimeNodes(int Nt,int Nst,double Dt,double Dt2,int ndofs=2){
		this.Nt=Nt
		this.Nst=Nst
		this.Dt=Dt
		this.Dt2=Dt2
		// setup nodes for domain
		int id=0;
		for (int j=1;j<=Ny;j++){
			double ycoord=(j-1)*hy+y0
			for (int i=1;i<=Nx;i++){
			      double xcoord=(i-1)*hx+x0
			      Node aNode=new Node(++id,xcoord,ycoord)
			      aNode.setNumDOFs_Steps(ndofs,Nst+1)
			      if(i==1||i==Nx||j==1||j==Ny)aNode.setBoundary() // set nodes belong to boundary
			      theDomain.putNode(aNode)
		      }
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	Closure inv4ACM = {double[][] A ->
		return Matrix(A).inverse().getData()
	}

	Closure InvAh={int i, int j ->
		double[][] iAH= new double[5][5];
		double da11=a11[i][j]; double da12=a12[i][j]; double da13=a13[i][j]
		double da22=a22[i][j]; double da23=a23[i][j]; double da33=a33[i][j];
		double cc=-2*da11*pow(da13,2)*pow(da22,2) + 4*da11*da12*da13*da22*da23 - 2*pow(da11,2)*da22*pow(da23,2) - 2*da11*pow(da12,2)*da22*da33 + 2*pow(da11,2)*pow(da22,2)*da33
	
		iAH[0][0]=(-(pow(da13,2)*pow(da22,2)) + 2*da12*da13*da22*da23 - 2*da11*da22*pow(da23,2) - pow(da12,2)*da22*da33 + 2*da11*pow(da22,2)*da33)/cc
		iAH[1][0]=(pow(da13,2)*pow(da22,2) - 2*da12*da13*da22*da23 + pow(da12,2)*da22*da33)/cc; iAH[0][1]=iAH[1][0]
		iAH[2][0]=(da11*da13*da22*da23 - da11*da12*da22*da33)/cc; iAH[0][2]=iAH[2][0]
		iAH[3][0]=(da11*da13*da22*da23 - da11*da12*da22*da33)/cc; iAH[0][3]=iAH[3][0]
		iAH[4][0]=(-(da11*da13*pow(da22,2)) + da11*da12*da22*da23)/cc; iAH[0][4]=iAH[4][0]
		
		iAH[1][1]=(-(pow(da13,2)*pow(da22,2)) + 2*da12*da13*da22*da23 - 2*da11*da22*pow(da23,2) - pow(da12,2)*da22*da33 + 2*da11*pow(da22,2)*da33)/cc
		iAH[2][1]=(da11*da13*da22*da23 - da11*da12*da22*da33)/cc; iAH[1][2]=iAH[2][1]
		iAH[3][1]=(da11*da13*da22*da23 - da11*da12*da22*da33)/cc; iAH[1][3]=iAH[3][1]
		iAH[4][1]=(-(da11*da13*pow(da22,2)) + da11*da12*da22*da23)/cc; iAH[1][4]=iAH[4][1]
		
		iAH[2][2]=(-2*da11*pow(da13,2)*da22 + 2*da11*da12*da13*da23 - pow(da11,2)*pow(da23,2) - da11*pow(da12,2)*da33 + 2*pow(da11,2)*da22*da33)/cc
		iAH[3][2]=(-2*da11*da12*da13*da23 + pow(da11,2)*pow(da23,2) + da11*pow(da12,2)*da33)/cc; iAH[2][3]=iAH[3][2]
		iAH[4][2]=(da11*da12*da13*da22 - pow(da11,2)*da22*da23)/cc; iAH[2][4]=iAH[4][2]
		
		iAH[3][3]=(-2*da11*pow(da13,2)*da22 + 2*da11*da12*da13*da23 - pow(da11,2)*pow(da23,2) - da11*pow(da12,2)*da33 + 2*pow(da11,2)*da22*da33)/cc
		iAH[4][3]=(da11*da12*da13*da22 - pow(da11,2)*da22*da23)/cc; iAH[3][4]=iAH[4][3]
		
		iAH[4][4]=(-(da11*pow(da12,2)*da22) + pow(da11,2)*pow(da22,2))/cc
		return iAH
	}

	Closure setStress={double[][] Str->
		double[] sol=new double[Nx*Ny]
		int i,j
		int count=0
		(0..<Ny).each{
			j=it
			(0..<Nx).each{
				i=it
				sol[count++]=Str[i][j]
			}
		}
		//Matrix(Str).print()
		//println sol
		return sol
	}
	
	Closure setVelc={double[][] varr ->
		double[] sol=new double[Nx*Ny]
		int i,j
		(0..<Ny-1).each{
			j=it
			(0..<Nx-1).each{
				i=it
				int k1=j*Nx+i
				int k2=k1+1
				int k3=k1+Nx
				int k4=k3+1
				sol[k1]+=varr[i][j]
				sol[k2]+=varr[i][j]
				sol[k3]+=varr[i][j]
				sol[k4]+=varr[i][j]
			}
		}
		int count=-1
		(0..<Ny).each{
			j=it
			(0..<Nx).each{
				i=it
				count++
				if(i==0||i==Nx-1||j==0||j==Ny-1){
					switch(i){
						case 0 : if(j==0||j==Ny-1){
							}else{
								sol[count]/=2.0;
								} 
								break;
						case (Nx-1) : 
						if(j==0||j==Ny-1){
						}else{
							sol[count]/=2.0;
						} 
						break;
					}
	
					switch(j){
						case 0 : if(i==0||i==Nx-1){
							}else{
								sol[count]/=2.0;
								} 
								break;
						case (Ny-1) : 
						if(i==0||i==Nx-1){
						}else{
							sol[count]/=2.0;
						} 
						break;
					}
				}else{
					sol[count]/=4.0
				}
			}
		}
		//Matrix(varr).print()
		//println("-------------------------------  -------------------------------")
		//println sol
		return sol
	}
	
	Closure setKinNrm={Vx,Vy->
		int i,j
		double[] sol=new double[Nx*Ny]
		double[] solx=new double[Nx*Ny]
		double[] soly=new double[Nx*Ny]
		(0..<Ny-1).each{
			j=it
			(0..<Nx-1).each{
				i=it
				int k1=j*Nx+i
				int k2=k1+1
				int k3=k1+Nx
				int k4=k3+1
				solx[k1]+=Vx[i][j]
				solx[k2]+=Vx[i][j]
				solx[k3]+=Vx[i][j]
				solx[k4]+=Vx[i][j]
				
				soly[k1]+=Vy[i][j]
				soly[k2]+=Vy[i][j]
				soly[k3]+=Vy[i][j]
				soly[k4]+=Vy[i][j]
			}
		}
		int count=-1
		(0..<Ny).each{
			j=it
			(0..<Nx).each{
				i=it
				count++
				if(i==0||i==Nx-1||j==0||j==Ny-1){
					switch(i){
						case 0 : if(j==0||j==Ny-1){
							}else{
								solx[count]/=2.0;
								soly[count]/=2.0;
								} 
								break;
						case (Nx-1) : 
						if(j==0||j==Ny-1){
						}else{
							solx[count]/=2.0;
							soly[count]/=2.0;
						} 
						break;
					}
	
					switch(j){
						case 0 : if(i==0||i==Nx-1){
							}else{
								solx[count]/=2.0;
								soly[count]/=2.0;
								} 
								break;
						case (Ny-1) : 
						if(i==0||i==Nx-1){
						}else{
							solx[count]/=2.0;
							soly[count]/=2.0;
						} 
						break;
					}
				}else{
					solx[count]/=4.0
					soly[count]/=4.0
				}
				sol[count]=sqrt(solx[count]*solx[count]+soly[count]*soly[count])
			}
		}
		return sol
	}
	
	Closure setKinDiv={Dx,Dy->
		int i,j
		double[][] Vx=new double[Nx-1][Ny-1]
		double[][] Vy=new double[Nx-1][Ny-1]
		(1..<Nx-2).each{
			i=it
			(1..<Ny-2).each{
				j=it
				Vx[i][j]=(Dx[i+1][j]-Dx[i-1][j])/(2.0*hx)
				Vy[i][j]=(Dy[i][j+1]-Dy[i][j-1])/(2.0*hy)
			}
		}
	
		i=0
		(1..<Ny-2).each{
			j=it
			Vx[i][j]=(Dx[i+1][j]-Dx[i][j])/hx
			Vy[i][j]=(Dy[i][j+1]-Dy[i][j-1])/(2.0*hy)
		}
	
		i=Nx-2
		(1..<Ny-2).each{
			j=it
			Vx[i][j]=(Dx[i][j]-Dx[i-1][j])/hx
			Vy[i][j]=(Dy[i][j+1]-Dy[i][j-1])/(2.0*hy)
		}
	
		j=0
		(1..<Nx-2).each{
			i=it
			Vx[i][j]=(Dx[i+1][j]-Dx[i-1][j])/(2.0*hx)
			Vy[i][j]=(Dy[i][j+1]-Dy[i][j])/hy
		}
	
		j=Ny-2
		(1..<Nx-2).each{
			i=it
			Vx[i][j]=(Dx[i+1][j]-Dx[i-1][j])/(2.0*hx)
			Vy[i][j]=(Dy[i][j]-Dy[i][j-1])/hy
		}
	
		i=0; j=0
		Vx[i][j]=(Dx[i+1][j]-Dx[i][j])/hx
		Vy[i][j]=(Dy[i][j+1]-Dy[i][j])/hy
	
		i=0; j=Ny-2
		Vx[i][j]=(Dx[i+1][j]-Dx[i][j])/hx
		Vy[i][j]=(Dy[i][j]-Dy[i][j-1])/hy
	
		i=Nx-2;j=0
		Vx[i][j]=(Dx[i][j]-Dx[i-1][j])/hx
		Vy[i][j]=(Dy[i][j+1]-Dy[i][j])/hy
	
		i=Nx-2;j=Ny-2
		Vx[i][j]=(Dx[i][j]-Dx[i-1][j])/hx
		Vy[i][j]=(Dy[i][j]-Dy[i][j-1])/hy
		
		double[] sol=new double[Nx*Ny]
		double[] solx=new double[Nx*Ny]
		double[] soly=new double[Nx*Ny]
		(0..<Ny-1).each{
			j=it
			(0..<Nx-1).each{
				i=it
				int k1=j*Nx+i
				int k2=k1+1
				int k3=k1+Nx
				int k4=k3+1
				solx[k1]+=Vx[i][j]
				solx[k2]+=Vx[i][j]
				solx[k3]+=Vx[i][j]
				solx[k4]+=Vx[i][j]
				
				soly[k1]+=Vy[i][j]
				soly[k2]+=Vy[i][j]
				soly[k3]+=Vy[i][j]
				soly[k4]+=Vy[i][j]
			}
		}
		int count=-1
		(0..<Ny).each{
			j=it
			(0..<Nx).each{
				i=it
				count++
				if(i==0||i==Nx-1||j==0||j==Ny-1){
					switch(i){
						case 0 : if(j==0||j==Ny-1){
							}else{
								solx[count]/=2.0;
								soly[count]/=2.0;
								} 
								break;
						case (Nx-1) : 
						if(j==0||j==Ny-1){
						}else{
							solx[count]/=2.0;
							soly[count]/=2.0;
						} 
						break;
					}
	
					switch(j){
						case 0 : if(i==0||i==Nx-1){
							}else{
								solx[count]/=2.0;
								soly[count]/=2.0;
								} 
								break;
						case (Ny-1) : 
						if(i==0||i==Nx-1){
						}else{
							solx[count]/=2.0;
							soly[count]/=2.0;
						} 
						break;
					}
				}else{
					solx[count]/=4.0
					soly[count]/=4.0
				}
				sol[count]=solx[count]+soly[count]
			}
		}
		return sol
	}
	
	Closure setKinRot={Dx,Dy->
		int i,j
		double[][] Vx=new double[Nx-1][Ny-1]
		double[][] Vy=new double[Nx-1][Ny-1]
		(1..<Nx-2).each{
			i=it
			(1..<Ny-2).each{
				j=it
				Vx[i][j]=(Dx[i][j+1]-Dx[i][j-1])/(2.0*hy)
				Vy[i][j]=(Dy[i+1][j]-Dy[i-1][j])/(2.0*hx)
			}
		}
	
		i=0
		(1..<Ny-2).each{
			j=it
			Vx[i][j]=(Dx[i][j+1]-Dx[i][j-1])/(2.0*hy)
			Vy[i][j]=(Dy[i+1][j]-Dy[i][j])/hx
		}
	
		i=Nx-2
		(1..<Ny-2).each{
			j=it
			Vx[i][j]=(Dx[i][j+1]-Dx[i][j-1])/(2.0*hy)
			Vy[i][j]=(Dy[i][j]-Dy[i-1][j])/hx
		}
	
		j=0
		(1..<Nx-2).each{
			i=it
			Vx[i][j]=(Dx[i][j+1]-Dx[i][j])/hy
			Vy[i][j]=(Dy[i+1][j]-Dy[i-1][j])/(2.0*hx)
		}
	
		j=Ny-2
		(1..<Nx-2).each{
			i=it
			Vx[i][j]=(Dx[i][j]-Dx[i][j-1])/hy
			Vy[i][j]=(Dy[i+1][j]-Dy[i-1][j])/(2.0*hx)
		}
	
		i=0; j=0
		Vx[i][j]=(Dx[i][j+1]-Dx[i][j])/hy
		Vy[i][j]=(Dy[i+1][j]-Dy[i][j])/hx
	
		i=0; j=Ny-2
		Vx[i][j]=(Dx[i][j]-Dx[i][j-1])/hy
		Vy[i][j]=(Dy[i+1][j]-Dy[i][j])/hx
	
		i=Nx-2;j=0
		Vx[i][j]=(Dx[i][j+1]-Dx[i][j])/hy
		Vy[i][j]=(Dy[i][j]-Dy[i-1][j])/hx
	
		i=Nx-2;j=Ny-2
		Vx[i][j]=(Dx[i][j]-Dx[i][j-1])/hy
		Vy[i][j]=(Dy[i][j]-Dy[i-1][j])/hx
			
		double[] sol=new double[Nx*Ny]
		double[] solx=new double[Nx*Ny]
		double[] soly=new double[Nx*Ny]
		(0..<Ny-1).each{
			j=it
			(0..<Nx-1).each{
				i=it
				int k1=j*Nx+i
				int k2=k1+1
				int k3=k1+Nx
				int k4=k3+1
				solx[k1]+=Vx[i][j]
				solx[k2]+=Vx[i][j]
				solx[k3]+=Vx[i][j]
				solx[k4]+=Vx[i][j]
				
				soly[k1]+=Vy[i][j]
				soly[k2]+=Vy[i][j]
				soly[k3]+=Vy[i][j]
				soly[k4]+=Vy[i][j]
			}
		}
		int count=-1
		(0..<Ny).each{
			j=it
			(0..<Nx).each{
				i=it
				count++
				if(i==0||i==Nx-1||j==0||j==Ny-1){
					switch(i){
						case 0 : if(j==0||j==Ny-1){
							}else{
								solx[count]/=2.0;
								soly[count]/=2.0;
								} 
								break;
						case (Nx-1) : 
						if(j==0||j==Ny-1){
						}else{
							solx[count]/=2.0;
							soly[count]/=2.0;
						} 
						break;
					}
	
					switch(j){
						case 0 : if(i==0||i==Nx-1){
							}else{
								solx[count]/=2.0;
								soly[count]/=2.0;
								} 
								break;
						case (Ny-1) : 
						if(i==0||i==Nx-1){
						}else{
							solx[count]/=2.0;
							soly[count]/=2.0;
						} 
						break;
					}
				}else{
					solx[count]/=4.0
					soly[count]/=4.0
				}
				sol[count]=solx[count]-soly[count]
			}
		}
		return sol
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	void doStep(int step, boolean store){
		//printf(domName)
		//printf(" the discrete time step %d\n",step)
		(1..<Nx-1).each{
			int i=it
			(1..<Ny-1).each{
				// compute the non-boundary stresses
				int j=it
				double B1x=(Vx[i][j]-Vx[i-1][j])
				double B2x=(Vx[i][j-1]-Vx[i-1][j-1])
				double B5x=(Vy[i][j]-Vy[i-1][j]+Vy[i][j-1]-Vy[i-1][j-1])
	
				double B3y=(Vy[i][j]-Vy[i][j-1])
				double B4y=(Vy[i-1][j]-Vy[i-1][j-1])
				double B5y=(Vx[i][j]-Vx[i][j-1]+Vx[i-1][j]-Vx[i-1][j-1])
				
				
				double[][] Bhx=new double[5][1]
				Bhx[0][0]=B1x/hx
				Bhx[1][0]=B2x/hx
				Bhx[4][0]=B5x/hy
	
				double[][] Bhy=new double[5][1]
				Bhy[2][0]=B3y/hy
				Bhy[3][0]=B4y/hy
				Bhy[4][0]=B5y/hx
	
				Matrix InvAhm = Matrix(InvAh(i,j))
				Matrix Bhxm=Matrix(Bhx)
				Matrix Bhym=Matrix(Bhy)
				Matrix InvABx=InvAhm*Bhxm
				Matrix InvABy=InvAhm*Bhym
				
				Sxxh[i][j]=Sxxh[i][j]+Dt*(InvABx.get(0,0)+InvABy.get(0,0))
				Sxxb[i][j]=Sxxb[i][j]+Dt*(InvABx.get(1,0)+InvABy.get(1,0))
				Syyd[i][j]=Syyd[i][j]+Dt*(InvABx.get(2,0)+InvABy.get(2,0))
				Syyg[i][j]=Syyg[i][j]+Dt*(InvABx.get(3,0)+InvABy.get(3,0))
				Sxy[i][j]=Sxy[i][j]+Dt*(InvABx.get(4,0)+InvABy.get(4,0))
			}
		}
		
		doBoundaryRelations(step)
		
		// velocities
		(0..<Nx-1).each{
			int i=it
			(0..<Ny-1).each{
				int j=it
				double dvpx=(Sxxh[i+1][j]-Sxxh[i][j]+Sxxb[i+1][j+1]-Sxxb[i][j+1]+Sxy[i][j+1]-Sxy[i][j]+Sxy[i+1][j+1]-Sxy[i+1][j])*Dt/(2.0*rho*hx)
				double dvpy=(Sxy[i+1][j]-Sxy[i][j]+Sxy[i+1][j+1]-Sxy[i][j+1]+Syyd[i][j+1]-Syyd[i][j]+Syyg[i+1][j+1]-Syyg[i+1][j])*Dt/(2.0*rho*hy)
				if(BodyForces){
					dvpx+=g[step]*fx[i][j]*Dt/rho
					dvpy+=g[step]*fy[i][j]*Dt/rho
				}
	
				//Dx[i][j]=Dx[i][j]+(2.0*Vx[i][j]+dvpx)*Dt/2.0
				//Dy[i][j]=Dy[i][j]+(2.0*Vy[i][j]+dvpy)*Dt/2.0
				Dx[i][j]=Dx[i][j]+Vx[i][j]*Dt+dvpx*Dt/2.0
				Dy[i][j]=Dy[i][j]+Vy[i][j]*Dt+dvpy*Dt/2.0
				
				Vx[i][j]=Vx[i][j]+dvpx
				Vy[i][j]=Vy[i][j]+dvpy
			}
		}
		doCloseStep(step)
		if(store)doStoreResponse(step)

		//triangulate() // to be removed, there is no need to be done in every time step
	}

	void configureLoading(){}
	
	void doCloseStep(int step){}

	void doBoundaryRelations(int step){
		//println domName+"original doBoundaryRelations"
	}

	void doStoreResponse(int step){
		if(step%storeStep == 0){
			rstep++
			theDomain.updateNodesDOF(setVelc(Dx),rstep,0)
			theDomain.updateNodesDOF(setVelc(Dy),rstep,1)
			//theDomain.updateNodesDOF(setVelc(Vx),rstep,2)
			//theDomain.updateNodesDOF(setVelc(Vy),rstep,3)
			theDomain.updateNodesDOF(setKinNrm(Vx,Vy),rstep,2)
			theDomain.updateNodesDOF(setKinDiv(Vx,Vy),rstep,3)
			theDomain.updateNodesDOF(setKinRot(Vx,Vy),rstep,4)
			
			//theDomain.updateNodesDOF(setStress(Syyd),rstep,2)
			//theDomain.updateNodesDOF(setStress(Syyg),rstep,3)
			//theDomain.updateNodesDOF(setStress(Sxy),rstep,4)
			//theDomain.updateNodesDOF(setStress(Sxxh),rstep,5)
			//theDomain.updateNodesDOF(setStress(Sxxb),rstep,6)
			//theDomain.updateNodesDOF(setKinDiv(Vx,Vy),rstep,7)
		}
	}

	void triangulate(){
		java.util.LinkedHashMap trng=[:]
		int indtr=0
		boolean right=true
		(1..<Nx).each{
			int ix=it
			(1..<Ny).each{
				int iy=it
				int nc=(iy-1)*Nx
				Node[] nds = new Node[3];
				right=true; if(random()<0.5)right=false
				if(right){
						nds[0]=theDomain.getNode(nc+ix); nds[1]=theDomain.getNode(nc+ix+1); nds[2]=theDomain.getNode(nc+ix+1+Nx)
				}else{
						nds[0]=theDomain.getNode(nc+ix); nds[1]=theDomain.getNode(nc+ix+1); nds[2]=theDomain.getNode(nc+ix+Nx)
					}
				Triangle aTrg = new Triangle(++indtr,nds)
				trng.put(aTrg.getID(), aTrg)
		
				nds = new Node[3];
				if(right){
						nds[0]=theDomain.getNode(nc+ix+Nx); nds[2]=theDomain.getNode(nc+ix+Nx+1); nds[1]=theDomain.getNode(nc+ix)
						//right=false
					}else{
						nds[0]=theDomain.getNode(nc+ix+Nx+1); nds[1]=theDomain.getNode(nc+ix+Nx); nds[2]=theDomain.getNode(nc+ix+1)
						//right=true
					}
				aTrg = new Triangle(++indtr,nds)
				trng.put(aTrg.getID(), aTrg)
			}
		}
		theDomain.setShapes(trng)
	}

}

new TsogkaElast2D()

