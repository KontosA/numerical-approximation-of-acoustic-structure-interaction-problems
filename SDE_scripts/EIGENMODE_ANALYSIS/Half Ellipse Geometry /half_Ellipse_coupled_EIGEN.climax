import sympmesher.*
import geom.Point
import jfem.*

theUniverse.cls()
aDomain=theUniverse.FEMDomain();
fluid=true
structure=true

Lbeam=1.0
L1=Lbeam/3.0
H1=L1
H2=H1
Ltot=Lbeam+2*L1

Rx=Lbeam/2.0+L1
Ry=H1+H2
x0=Rx

Xref=0.0*Ltot; Yref=2.5*H1

mesh1_1=15; mesh1_2=15
mesh2_1=15; mesh2_2=mesh1_2
mesh3_1=mesh1_1; mesh3_2=mesh2_1
mesh4_1=mesh2_1; mesh4_2=mesh1_2
internalEllipse=true

ide=0
if(fluid){
	Geom=new Area2D()
	Geom.initglobalIndex()
	
	Points=[]
	Points.add(new Point(Xref+0.0,Yref+0.0)) // P0
	Points.add(new Point(Xref+L1/2.0,Yref+0.0)) // P1
	Points.add(new Point(Xref+L1,Yref+0.0)) // P2
	Points.add(new Point(Xref+L1+Lbeam/2.0,Yref+0.0)) // P3
	Points.add(new Point(Xref+L1+Lbeam,Yref+0.0)) // P4
	Points.add(new Point(Xref+3*L1/2+Lbeam,Yref+0.0)) //P5
	Points.add(new Point(Xref+2*L1+Lbeam,Yref+0.0)) //P6
	
	Points.add(new Point(Xref+x0+Rx*cos(PI+PI/6.0),Yref+Ry*sin(PI+PI/6.0))) // P7
	if(internalEllipse){Points.add(new Point(Xref+x0+0.5*Lbeam*cos(PI+PI/6.0),Yref+H1*sin(PI+PI/6.0)))} // P8
	else{Points.add(new Point(0.5*(Points[7].X()+Points[3].X()),0.5*(Points[7].Y()+Points[3].Y())))} // P8
	
	Points.add(new Point(Xref+x0+Rx*cos(PI+2.0*PI/6.0),Yref+Ry*sin(PI+2.0*PI/6.0))) // P9
	Points.add(new Point(Xref+x0+0.5*Lbeam*cos(PI+2.0*PI/6.0),Yref+H1*sin(PI+2.0*PI/6.0))) // P10
	Points.add(new Point(Xref+x0+0.5*(0.5*Lbeam*cos(PI+2.0*PI/6.0)+Rx*cos(PI+2.0*PI/6.0)), Yref+0.5*(H1*sin(PI+2.0*PI/6.0)+Ry*sin(PI+2.0*PI/6.0)))) // P11
	
	Points.add(new Point(Xref+x0+Rx*cos(PI+3.0*PI/6.0),Yref+Ry*sin(PI+3.0*PI/6.0))) // P12
	Points.add(new Point(Xref+x0+0.5*Lbeam*cos(PI+3.0*PI/6.0),Yref+H1*sin(PI+3.0*PI/6.0))) // P13
	
	Points.add(new Point(Xref+x0+Rx*cos(PI+4.0*PI/6.0),Yref+Ry*sin(PI+4.0*PI/6.0))) // P14
	Points.add(new Point(Xref+x0+0.5*Lbeam*cos(PI+4.0*PI/6.0),Yref+H1*sin(PI+4.0*PI/6.0))) // P15
	Points.add(new Point(Xref+x0+0.5*(0.5*Lbeam*cos(PI+4.0*PI/6.0)+Rx*cos(PI+4.0*PI/6.0)),Yref+0.5*(H1*sin(PI+4.0*PI/6.0)+Ry*sin(PI+4.0*PI/6.0)))) // P16
	
	Points.add(new Point(Xref+x0+Rx*cos(PI+5.0*PI/6.0),Yref+Ry*sin(PI+5.0*PI/6.0))) // P17
	if(internalEllipse){Points.add(new Point(Xref+x0+0.5*Lbeam*cos(PI+5.0*PI/6.0),Yref+H1*sin(PI+5.0*PI/6.0)))} // P18
	else{Points.add(new Point(0.5*(Points[17].X()+Points[3].X()),0.5*(Points[17].Y()+Points[3].Y())))} // P18
	
	//Points.each{it.print()}
	
	Geom.putCurve3P(new Curve3P(1, Points[0], Points[1], Points[2]))
	Geom.putCurve3P(new Curve3P(2, Points[2], Points[3], Points[4]))
	Geom.putCurve3P(new Curve3P(3, Points[4], Points[5], Points[6]))
	Geom.putCurve3P(new Curve3P(4, Points[2], Points[8], Points[10]))
	Geom.putCurve3P(new Curve3P(5, Points[10], Points[13], Points[15]))
	Geom.putCurve3P(new Curve3P(6, Points[15], Points[18], Points[4]))
	Geom.putCurve3P(new Curve3P(7, Points[9], Points[11], Points[10]))
	Geom.putCurve3P(new Curve3P(8, Points[14], Points[16], Points[15]))
	Geom.putCurve3P(new Curve3P(9, Points[9], Points[7], Points[0]))
	Geom.putCurve3P(new Curve3P(10, Points[14], Points[12], Points[9]))
	Geom.putCurve3P(new Curve3P(11, Points[6], Points[17], Points[14]))
	
	aPatch= new Patch(1, Geom.getCurve3P(2), Geom.getCurve3P(6), Geom.getCurve3P(5), Geom.getCurve3P(4), mesh1_1, mesh1_2)
	aPatch.setReversedCurve(2); aPatch.setReversedCurve(3); aPatch.setReversedCurve(4)
	Geom.putPatch(aPatch)
	
	aPatch= new Patch(2, Geom.getCurve3P(1), Geom.getCurve3P(4), Geom.getCurve3P(7), Geom.getCurve3P(9), mesh2_1, mesh2_2)
	aPatch.setReversedCurve(3)
	Geom.putPatch(aPatch)
	
	aPatch= new Patch(3, Geom.getCurve3P(5), Geom.getCurve3P(8), Geom.getCurve3P(10), Geom.getCurve3P(7), mesh3_1, mesh3_2)
	aPatch.setReversedCurve(2)
	Geom.putPatch(aPatch)
	
	aPatch= new Patch(4, Geom.getCurve3P(3), Geom.getCurve3P(11), Geom.getCurve3P(8), Geom.getCurve3P(6), mesh4_1, mesh4_2)
	Geom.putPatch(aPatch)
	
	Geom.dothemesh()
	
	Geom.getPoints().each { key, value ->
		aDomain.putNode(new Node(key,value.X(),value.Y()))
	}
	
	dens=1000.0; speed=1500.0
	elast=speed*speed*dens
	aMat= new AcousticMaterial(1,elast,dens)
	aDomain.putMaterial(aMat)
	
	// define cross-sections
	aSect= new CrossSection(1);
	aSect.setThickness(1.0)
	aDomain.putCrossSection(aSect);
	
	Geom.getquads().each {
		elem =new TwoDAcousticQuad(++ide, aDomain.getNode(it.getN4()),aDomain.getNode(it.getN3()),aDomain.getNode(it.getN2()),aDomain.getNode(it.getN1()),
		                aDomain.getMaterial(1),aDomain.getCrossSection(1));
		aDomain.putElement(elem);
		println elem.getID()+" "+elem.getNodeHierarchy(1).getID()+" "+elem.getNodeHierarchy(2).getID()+" "+elem.getNodeHierarchy(3).getID()+" "+elem.getNodeHierarchy(4).getID()
	}
}else{
	(0..mesh1_1).each{
		y=Yref; x=Xref+L1+it*Lbeam/mesh1_1
		aDomain.putNode(new Node(it+1,x,y))
	}
}
/////////////////////////////////////////////////////////////
// structure (beam) part
////////////////////////////////////////////////////////////
if(structure){
	// it is supposed to be from node 1 to node mesh1_1+1
	//crossSection of first material
	b=0.25; h=0.5
	A=b*h
	As=0.0
	Imi=b*h*h*h/12.0
	solidSect= new CrossSection(2, A, As, Imi)
	
	//define first material
	Elast=2.1E11//1.0E9
	dens=2500.0//950.0
	solidMat= new ElasticMaterial(3,Elast)
	solidMat.setDensity(dens)
	
	// define elements
	N=mesh1_1
	nn=ide
	(1..N).each{
		elem =new EBeam2d(++ide, aDomain.getNode(it),aDomain.getNode(it+1),solidMat,solidSect)
		aDomain.putElement(elem)
	}
	idc=0
	// boundary conditions
	mc=aDomain.getMaxKMcomponent();
	Ak=10.0e14; Am=Ak*mc[1]/mc[0]
	
	aCE = new ConstraintElement(++idc,1.0, aDomain.getNode(1), 1)
	aCE.setAk(Ak); aCE.setAm(Am)
	aDomain.putConstraintElement(aCE)
	
	aCE = new ConstraintElement(++idc,1.0, aDomain.getNode(mesh1_1+1), 1)
	aCE.setAk(Ak); aCE.setAm(Am)
	aDomain.putConstraintElement(aCE)
	
	aCE = new ConstraintElement(++idc,1.0, aDomain.getNode(1), 2)
	aCE.setAk(Ak); aCE.setAm(Am)
	aDomain.putConstraintElement(aCE)
	
	aCE = new ConstraintElement(++idc,1.0, aDomain.getNode(mesh1_1+1), 2)
	aCE.setAk(Ak); aCE.setAm(Am)
	aDomain.putConstraintElement(aCE)
	
	//aCE = new ConstraintElement(++idc,1.0, aDomain.getNode(1+ff), 6)
	//aCE.setAk(Ak); aCE.setAm(Am)
	//aDomain.putConstraintElement(aCE)
	
	//aCE = new ConstraintElement(++idc,1.0, aDomain.getNode(N+1+ff), 6)
	//aCE.setAk(Ak); aCE.setAm(Am)
	//aDomain.putConstraintElement(aCE)
}

aDomain.activateConsMass()

// Coupling
couple=true
if(!structure || !fluid)couple=false
if(couple){
	Coupling2dBeam2dAcoustQuad.mode=0 // 0: p-u, 1: psi-u
	idc=0
	(1..mesh1_1).each{
		coupling= new Coupling2dBeam2dAcoustQuad(++idc, aDomain.getElement(it+nn),aDomain.getElement(it))
		aDomain.putCouplingElement(coupling)
	}
}

// define analysis
Analysis theAnalysis = new EigenAnalysis(aDomain,3)
theAnalysis.analyse();
theAnalysis.endAnalysis();

//aDomain.printEigenValues() // the domain will print eigenvalues, eigenfrequencies, eigenperiods
femEig=aDomain.getEigenValues() 

// it gets only eigenvalues in femEig
// from eigenvalue lambda to eigenfrequency f: f=sqrt(lambda)/(2*PI)
cc=0
femEig.each{
	println "f_"+(++cc)+" = "+sqrt(it)/(2*PI)+" Hz"
	//println "omega_"+(++cc)+" = "+sqrt(it)+" rad/sec"
}

eig=31
mm=Double.NEGATIVE_INFINITY
part=1
(1..mesh1_1+1).each{
	disps=aDomain.getNode(it).getLoadCaseDisps(-eig)
	if(disps[1]>mm)mm=disps[1]
	if(disps[0]>mm)mm=disps[0]
	//disps.each{if(it>mm)mm=it}
}
scale=1000.0
domavar={jfem.Domain dom, int nid, int LC, int ts ->
	double val=abs(dom.getNode(nid).getLoadCaseDisps(LC,ts)[6]);
	return val
}
theGP.colormode=0
//domavar=null
theGP.ContourVar = domavar as jfem.DomainVariable
//theGP.ContourVar = null
theGP.contourDisp(-eig)
theGP.plotDeform(-eig,scale)
theGP.title=" f_ "+(eig)+"="+sqrt(femEig[eig-1])/(2*PI)+"Hz"
//theGP.stop()

println (aDomain.getNumElement())

x=[]
y=[]
(0..mesh1_1).each{
	xc=L1+it*Lbeam/mesh1_1
	yc=Yref
	nod=aDomain.find(xc, yc)
	x.add(aDomain.getNode(nod).X())
	y.add(aDomain.getNode(nod).getLoadCaseDisps(-eig)[1])
}
xPlot= new PlotFrame()
xPlot.addFunction(new plotfunction(x as double[], y as double[]))
xPlot.setMarker(true)
xPlot.show()

/*
PBNodes = Geom.getBoundaryPoints()
equat={x,y-> return abs((x-x0-Xref)*(x-x0-Xref)/(Rx*Rx)+(y-Yref)*(y-Yref)/(Ry*Ry)) }
BoundaryNodes=[];
PBNodes.each{
	val=equat(it.value.X(),it.value.Y())
	
	if(abs(val-1.0)<0.01)BoundaryNodes.add(it.key)

	if(abs(it.value.Y()-Yref)<0.01 && !(BoundaryNodes.contains(it.key)))BoundaryNodes.add(it.key)
}


println "========================"
BoundaryNodes.each{println it}

*/

/*
(31..100).each{
	theGP.contourDisp(-it)
	theGP.plotDeform(-it,scale)
	theGP.title=" f_ "+(it)+"="+sqrt(femEig[it-1])/(2*PI)+"Hz"
	theGP.saveImageSelection("C:/Users/Kontos A/Desktop/weak_stucture/coupled_weak_str/eigs/eig_"+it+".png")
	
}
*/

